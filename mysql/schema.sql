-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema incidentes
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema incidentes
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `incidentes` DEFAULT CHARACTER SET utf8 ;
USE `incidentes` ;

-- -----------------------------------------------------
-- Table `incidentes`.`tipo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `incidentes`.`tipo` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;

INSERT INTO `tipo` (`id`,`nome`) VALUES (1,'Ataque Brute Force');
INSERT INTO `tipo` (`id`,`nome`) VALUES (2,'Credencias vazadas');
INSERT INTO `tipo` (`id`,`nome`) VALUES (3,'Ataque de DDoS');
INSERT INTO `tipo` (`id`,`nome`) VALUES (4,'Atividades anormais de usuários');

-- -----------------------------------------------------
-- Table `incidentes`.`cadastro`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `incidentes`.`cadastro` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `id_tipo` INT(11) NOT NULL,
  `titulo` VARCHAR(45) NOT NULL,
  `descricao` TEXT NOT NULL,
  `criticidade` ENUM('alta', 'média', 'baixa') NOT NULL,
  `status` TINYINT(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  INDEX `cadastro_x_tipo_idx` (`id_tipo` ASC),
  CONSTRAINT `cadastro_x_tipo`
    FOREIGN KEY (`id_tipo`)
    REFERENCES `incidentes`.`tipo` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 2
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
