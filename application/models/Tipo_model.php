<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Created by PhpStorm.
 * User: celso
 * Date: 09/01/2019
 * Time: 10:51
 */

class Tipo_model extends CI_Model
{
    public function findAll()
    {
        $this->db->order_by('nome', 'ASC');
        return $this->db->get('tipo')->result();
    }

    public function findById($id)
    {
        try {
            if (is_null($id)) {
                throw new Exception('Id não enviado', 400);
            }

            $this->db->where('id', $id);
            return $this->db->get('tipo')->row();
        } catch (Exception $e) {
            return false;
        }
    }
}
