<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cadastro extends CI_Controller
{
    private $cadastros;
    private $tipos;
    private $regex = '/(\()?(10)|([1-9]){2}\)?((-|\s)?)([2-9][0-9]{3}((-|\s)?)[0-9]{4,5})/';

    public function __construct()
    {
        parent::__construct();
        $this->load->model('cadastro_model', 'modelcadastro');
        $this->load->model('tipo_model', 'modeltipo');

        $this->cadastros = $this->modelcadastro->findAll();
        $this->tipos = $this->modeltipo->findAll();
    }

    public function index()
	{
        $data_header['menu'] = 'hom';
        $data_page['cadastros'] = $this->cadastros;
        $data_page['tipos'] = $this->tipos;

        $this->load->view('base/header', $data_header);
        $this->load->view('lista_cadastro', $data_page);
        $this->load->view('base/footer');
	}

    public function adicionar()
    {
        $data_header['menu'] = 'cad';
        $data_page['tipos'] = $this->tipos;

        $this->load->view('base/header', $data_header);
        $this->load->view('cadastro', $data_page);
        $this->load->view('base/footer');
	}

    public function salvar()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descricao', 'Descrição', 'required');
        $this->form_validation->set_rules('criticidade', 'Criticidade', 'required');
        $this->form_validation->set_rules('tipo', 'Tipo', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        if ($this->form_validation->run() == false) {
            $this->adicionar();
        }
        else {
            $dados['titulo']      = $this->input->post('titulo');
            $dados['status']      = $this->input->post('status');
            $dados['id_tipo']     = $this->input->post('tipo');
            $dados['descricao']   = $this->input->post('descricao');
            $dados['criticidade'] = $this->input->post('criticidade');

            if ($this->modelcadastro->insert($dados)) {
                $this->session->set_flashdata('success', 'Cadastro efetuado com sucesso!');
                redirect('cadastro');
            }

            $this->session->set_flashdata('error', 'Cadastro não pode ser efetuado!');
            redirect('cadastro/');
        }
	}

	public function editar($id)
    {
        $data_header['menu'] = 'cad';
        $data_page['cadastro'] = $this->modelcadastro->findById($id);
        $data_page['tipos'] = $this->modeltipo->findAll($id);

        $this->load->view('base/header', $data_header);
        $this->load->view('editar_cadastro', $data_page);
        $this->load->view('base/footer');
    }

    public function alterar()
    {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('titulo', 'Título', 'required');
        $this->form_validation->set_rules('descricao', 'Descrição', 'required');
        $this->form_validation->set_rules('criticidade', 'Criticidade', 'required');
        $this->form_validation->set_rules('tipo', 'Tipo', 'required');
        $this->form_validation->set_rules('status', 'Status', 'required');
        if ($this->form_validation->run() == false) {
            $this->editar($this->input->post('id'));
        }
        else {
            $dados['titulo']      = $this->input->post('titulo');
            $dados['status']      = $this->input->post('status');
            $dados['id_tipo']     = $this->input->post('tipo');
            $dados['descricao']   = $this->input->post('descricao');
            $dados['criticidade'] = $this->input->post('criticidade');

            if ($this->modelcadastro->editar($this->input->post('id'), $dados)) {
                $this->session->set_flashdata('success', 'Cadastro editado com sucesso!');
                redirect('cadastro');
            }

            $this->session->set_flashdata('error', 'Cadastro não pode ser editado!');
            redirect('cadastro/editar/'.$this->input->post('id'));
        }
    }

    public function ver($id)
    {
        $data_header['menu'] = 'hom';
        $data_page['cadastro'] = $this->modelcadastro->findById($id);
        $data_page['tipo'] = $this->modeltipo->findById($data_page['cadastro']->id_tipo);

        $this->load->view('base/header', $data_header);
        $this->load->view('cad_ver', $data_page);
        $this->load->view('base/footer');
	}

    public function excluir($id)
    {
        if (!$this->modelcadastro->findById($id)) {
            $this->session->set_flashdata('error', '1 Cadastro não pode ser excluido!');
            redirect('cadastro');
        }

        if ($this->modelcadastro->excluir($id)) {
            $this->session->set_flashdata('success', 'Cadastro excluido com sucesso!');
        } else {
            $this->session->set_flashdata('error', '2 Cadastro não pode ser excluido!');
        }

        redirect('cadastro');
    }
}
