<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row marketing">
    <div class="col-lg-12">
        <dl class="dl-horizontal">
            <dt>Título</dt>
            <dd><?php echo $cadastro->titulo ?></dd>

            <dt>Descrição</dt>
            <dd><?php echo $cadastro->descricao ?></dd>

            <dt>Criticidade</dt>
            <dd><?php echo $cadastro->criticidade ?></dd>

            <dt>Tipo</dt>
            <dd><?php echo $tipo->nome ?></dd>

            <dt>Status</dt>
            <dd><?php echo ($cadastro->status == 1) ? 'Aberto' : 'Fechado' ?></dd>
        </dl>

        <a class="btn btn-default btn-xs" href="javascript:history.go(-1);" role="button">
            <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
            <span class="glyphicon-class">Voltar</span>
        </a>
        <a class="btn btn-default btn-xs" href="<?php echo base_url('cadastro/editar/' . $cadastro->id); ?>" role="button">
            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
            <span class="glyphicon-class">Editar</span>
        </a>
        <a class="btn btn-default btn-xs" href="<?php echo base_url('cadastro/excluir/' . $cadastro->id); ?>" role="button">
            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
            <span class="glyphicon-class">Excluir</span>
        </a>
    </div>
</div>
