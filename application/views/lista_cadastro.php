<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row marketing">
    <div class="col-lg-12">
        <?php if (!is_null($this->session->flashdata('success'))) : ?>
            <div class="alert alert-success alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Sucesso!</strong> <?php echo $this->session->flashdata('success') ?>
            </div>
        <?php endif; ?>

        <?php if (!is_null($this->session->flashdata('error'))) : ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Ops!</strong> <?php echo $this->session->flashdata('error') ?>
            </div>
        <?php endif; ?>

        <table class="table table-striped">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Título</th>
                    <th>Descrição</th>
                    <th>Criticidade</th>
                    <th>Tipo</th>
                    <th>Status</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach ($cadastros as $cadastro): ?>
                <tr>
                    <th scope="row"><?php echo $cadastro->id ?></th>
                    <td><?php echo $cadastro->titulo ?></td>
                    <td><?php echo character_limiter($cadastro->descricao, 20, '...') ?></td>
                    <td><?php echo ucfirst($cadastro->criticidade) ?></td>

                    <?php
                        $type = '';
                        foreach ($tipos as $tipo) {
                            if ($cadastro->id_tipo == $tipo->id) {
                                $type = $tipo->nome;
                                break;
                            }
                        }
                    ?>

                    <td><?php echo $type ?></td>
                    <td><?php echo ($cadastro->status == 1) ? 'Aberto' : 'Fechado' ?></td>
                    <td>
                        <a class="btn btn-default btn-xs" href="<?php echo base_url('cadastro/ver/' . $cadastro->id); ?>" role="button">
                            <span class="glyphicon glyphicon-search" aria-hidden="true"></span>
                            <span class="glyphicon-class">Ver</span>
                        </a>
                        <a class="btn btn-default btn-xs" href="<?php echo base_url('cadastro/editar/' . $cadastro->id); ?>" role="button">
                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                            <span class="glyphicon-class">Editar</span>
                        </a>
                        <a class="btn btn-default btn-xs" href="<?php echo base_url('cadastro/excluir/' . $cadastro->id); ?>" role="button">
                            <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                            <span class="glyphicon-class">Excluir</span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
