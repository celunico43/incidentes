<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="row marketing">
    <div class="col-lg-12">
        <?php if (!is_null($this->session->flashdata('error'))) : ?>
            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <strong>Ops!</strong> <?php echo $this->session->flashdata('error') ?>
            </div>
        <?php endif; ?>

        <?php echo validation_errors(); ?>

        <form method="post" action="<?php echo base_url('cadastro/salvar') ?>" id="form_cadastro">
            <div class="form-group">
                <label for="inputTitulo">Título</label>
                <input type="text" class="form-control" id="inputTitulo" name="titulo"
                       placeholder="Título" value="<?php echo set_value('titulo') ?>">
            </div>
            <div class="form-group">
                <label for="exampleInputDescricao">Descrição</label>
                <textarea class="form-control" name="descricao"
                          id="exampleInputDescricao" cols="30"
                          rows="10" value="<?php echo set_value('descricao') ?>"></textarea>
            </div>
            <div class="form-group">
                <label for="exampleInputCriticidade">Criticidade</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="criticidade" id="optionsCritical1" value="alta" checked>
                        Alta
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="criticidade" id="optionsCritical2" value="média">
                        Média
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="criticidade" id="optionsCritical3" value="baixa">
                        Baixa
                    </label>
                </div>
            </div>
            <div class="form-group">
                <label for="exampleInputTipo">Tipo</label>
                <select name="tipo" id="exampleInputTipo" class="form-control">
                    <option value="">Selecione</option>
                    <?php foreach ($tipos as $tipo): ?>
                    <option value="<?php echo $tipo->id ?>"><?php echo $tipo->nome ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleInputStatus">Status</label>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="optionsStatus1" value="1" checked>
                        Aberto
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" name="status" id="optionsStatus2" value="0">
                        Fechado
                    </label>
                </div>
            </div>
            <button type="submit" class="btn btn-default">Cadastrar</button>
        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $("#exampleInputPhone").mask('(00)0000.00000');
        $("#exampleInputRg").mask('00.000.000-0', {reverse: true});

        $("#exampleInputCep").mask('00000-000', {reverse: true});
        $("#exampleInputCep").blur(function () {
            $.getJSON("https://viacep.com.br/ws/"+ $("#exampleInputCep").val() +"/json",
            function (dados) {
                if (!("erro" in dados)) {
                    $("#exampleInputAddress").val(dados.logradouro);
                    $("#exampleInputNumber").focus();
                }
                else {
                    alert("CEP não encontrado.");
                }
            });
        });

        $("#form_cadastro").submit(function() {
            $("#exampleInputCep").unmask();
            $("#exampleInputPhone").unmask();
            $("#exampleInputRg").unmask();
        });
    });
</script>